module.exports = {
    root: true,
    env: {
        browser: true,
        node: true,
    },

    extends: [
        "next/core-web-vitals",
        "airbnb",
    ],

    parserOptions: {
        project: "./tsconfig.json",
    },

    globals: {
        NodeJS: true,
        JSX: true,
    },

    rules: {
        "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
        "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",

        indent: ["error", 4],

        "max-len": ["error", 140],
        quotes: ["error", "double"],
        semi: ["error", "never"],
        "no-unused-vars": "warn",
        "import/no-named-as-default": "off",
        "import/extensions": ["error", "never"],
        "import/prefer-default-export": "off",

        "react/require-default-props": "off",
        "jsx-a11y/anchor-is-valid": "off",
        "jsx-a11y/no-static-element-interactions": "off",
        "jsx-a11y/click-events-have-key-events": "off",

        "react/jsx-indent": ["error", 4],
        "react/jsx-indent-props": ["error", 4],
        "react/jsx-filename-extension": ["warn", { extensions: [".js", ".tsx"] }],

        "react/jsx-one-expression-per-line": "off",

        "react/prop-types": [2, { ignore: ["children"] }],
        "react/forbid-prop-types": "off",

        "no-param-reassign": [
            "error",
            { props: true, ignorePropertyModificationsFor: ["state"] },
        ],

        "import/no-extraneous-dependencies": [
            "error",
            {
                devDependencies: [
                    // Rule is ignored for these files
                    "/src/setupTests.js",
                ],
                optionalDependencies: false,
                peerDependencies: false,
            },
        ],
    },
}
