import { useIsInViewPort } from "@/hooks"
import classNames from "classnames"
import React, {
    ReactNode, useRef,
} from "react"

const transitions = {
    fade: "opacity-0",
}

type Props = {
    transition: keyof typeof transitions,
    children: ReactNode,
}

export function AnimateIn({ transition, children }: Props) {
    const parentRef = useRef<HTMLDivElement>(null)
    const isVisible = useIsInViewPort(parentRef, 0.2)
    return (
        <div
            ref={parentRef}
            className={classNames(
                "transition ease-in duration-500",
                { [transitions[transition]]: !isVisible },
            )}
        >
            {children}
        </div>
    )
}

export default AnimateIn
