/* eslint-disable react/jsx-props-no-spreading */
import classNames from "classnames"
import React, {
    ComponentType, ReactNode, MouseEventHandler,
} from "react"

const variants = {
    orange: `block text-center px-8 py-2 shadow rounded-lg 
    text-white cursor-pointer bg-orange hover:bg-orange-70 active:bg-orange-70 
    transition-colors`,
}

type Props<TAG extends keyof JSX.IntrinsicElements> = {
    component?: ComponentType | keyof JSX.IntrinsicElements
    variant?: keyof typeof variants
    disabled?: boolean
    onClick?: MouseEventHandler
    className?: string
    children?: ReactNode
} & JSX.IntrinsicElements[TAG]

export function Button<TAG extends keyof JSX.IntrinsicElements = "button">({
    component: ButtonComponent = "button",
    variant = "orange",
    disabled = false,
    onClick = undefined,
    className = undefined,
    children,
}: Props<TAG>) {
    return (
        <ButtonComponent
            className={classNames(
                variants[variant],
                className,
            )}
            onClick={onClick}
            disabled={disabled}
        >
            {children}
        </ButtonComponent>
    )
}

export default Button
