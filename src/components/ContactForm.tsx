import React, { ChangeEvent, useReducer, useState } from "react"
import qs from "qs"
import axios from "axios"
import Typo from "./Typo"
import AnimateIn from "./AnimateIn"
import FormField from "./FormField"
import Button from "./Button"
// import { Bounce, Fade } from "react-awesome-reveal"

interface FormData {
    name: string
    email: string
    message: string
}

interface UpdateFieldData {
    field: string
    value: string
}

function ContactForm() {
    const [state, dispatch] = useReducer(
        (s: FormData, { field, value }: UpdateFieldData) => ({ ...s, [field]: value }),
        {
            name: "", email: "", message: "",
        },
    )
    const [isSubmitting, setIsSubmitting] = useState(false)
    const [error, setError] = useState<any>(undefined)
    const [isSuccess, setIsSuccess] = useState(false)

    const onChange = (e: ChangeEvent<HTMLInputElement>) => {
        dispatch({ field: e.target.name, value: e.target.value })
    }

    const onSubmit = () => {
        setError(undefined)
        setIsSubmitting(true)
        return axios.post(
            "/api/contact",
            qs.stringify(state),
        )
            .then(() => { setIsSuccess(true) })
            .catch((e) => { setError(e) })
            .finally(() => { setIsSubmitting(false) })
    }

    const { name, email, message } = state
    let errorMessage
    if (error) {
        if (error.response && error.response.data && error.response.data.error) {
            errorMessage = error.response.data.error
        } else {
            errorMessage = error.message
        }
    }
    return (
        <div className="container mx-auto px-4 max-w-2xl">
            {!!errorMessage && (
                <div>
                    <Typo className="p-2 mb-8 bg-accent rounded shadow">{errorMessage}</Typo>
                </div>
            )}
            {/* <Fade right when={!!errorMessage} duration={250} mountOnEnter unmountOnExit>

            </Fade> */}

            <form
                onSubmit={(e) => {
                    e.preventDefault()
                    return onSubmit()
                }}
            >
                {isSuccess && (
                    <div className="">
                        <Typo variant="h3" gutterBottom>Thank you!</Typo>
                        <Typo gutterBottom>
                            Your message was sent successfully.<br />
                            I will contact you as soon as possible.
                        </Typo>
                    </div>
                )}

                {!isSuccess && (
                    <AnimateIn transition="fade">
                        <div>
                            <FormField
                                label="Name"
                                type="text"
                                name="name"
                                disabled={isSubmitting}
                                value={name}
                                onChange={onChange}
                            />
                            <FormField
                                label="E-mail"
                                type="email"
                                name="email"
                                disabled={isSubmitting}
                                value={email}
                                onChange={onChange}
                            />
                            <FormField
                                label="Message"
                                type="textarea"
                                name="message"
                                disabled={isSubmitting}
                                value={message}
                                onChange={onChange}
                            />
                            <Button
                                disabled={isSubmitting}
                                // showLoader={isSubmitting}
                                className="float-right clearfix"
                            >
                                Submit
                            </Button>
                        </div>
                    </AnimateIn>
                )}
            </form>
        </div>
    )
}

export default ContactForm
