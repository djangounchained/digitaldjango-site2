/* eslint-disable react/jsx-props-no-spreading */
import React, { ChangeEventHandler } from "react"
import Typo from "./Typo"

type Props = {
    type?: "textarea" | "text" | "email"
    value: string
    onChange: ChangeEventHandler
    label?: string
    name: string
    disabled?: boolean
}

function FormField({
    type = "text",
    value,
    onChange,
    label = undefined,
    name,
    disabled = false,
}: Props) {
    const Component = type === "textarea" ? "textarea" : "input"
    return (
        <div className="mb-4">
            { label && (
                <label htmlFor={name}>
                    <Typo>{label}</Typo>
                </label>
            )}
            <Component
                id={name}
                className="bg-black10 block w-full p-2 text-black shadow"
                name={name}
                value={value}
                onChange={onChange}
                type={type}
                disabled={disabled}
            />
        </div>
    )
}

export default FormField
