import { useParallax } from "@/hooks"
import React, {
    useRef, ReactNode,
} from "react"

type Props = {
    children: ReactNode,
}

export function HomeHeader({
    children,
}: Props) {
    const parallaxBack = useRef<HTMLDivElement>(null)
    const parallaxFront = useRef<HTMLDivElement>(null)

    useParallax(parallaxBack, 1.5)
    useParallax(parallaxFront, 2)

    return (
        <div className="h-header w-full relative overflow-hidden">
            <div ref={parallaxBack} className="absolute z-10 h-full w-full bg-gradient-to-br from-purple to-orange" />

            <div ref={parallaxFront} className="absolute z-20 h-full w-full flex items-center justify-center">
                <div className="p-4 text-white">
                    {children}
                </div>
            </div>
        </div>
    )
}

export default HomeHeader
