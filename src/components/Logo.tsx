import React from "react"

type Props = {
    size?: number
}

function Logo({
    size = 24,
}: Props) {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width={size}
            height={size}
            fill="none"
            viewBox="0 0 24 24"
        >
            <path
                fill="#FBF9F9"
                stroke="url(#paint0_linear_760_18)"
                strokeWidth="2"
                d="M5 6.554l7-4.375 7 4.375v10.892l-7 4.375-7-4.375V6.554z"
            />
            <defs>
                <linearGradient
                    id="paint0_linear_760_18"
                    x1="8.59"
                    x2="23.66"
                    y1="1.88"
                    y2="23.88"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#444054" />
                    <stop offset="1" stopColor="#FA824C" />
                </linearGradient>
            </defs>
        </svg>
    )
}

export default Logo
