import React, { ReactNode } from "react"
import Typo from "./Typo"

type Props = {
    children: ReactNode,
}

export function MenuButton({
    children,
}: Props) {
    return (
        <Typo className="p-2 rounded hover:bg-orange-70">
            {children}
        </Typo>
    )
}

export default MenuButton
