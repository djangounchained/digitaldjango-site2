import classNames from "classnames"
import React, { useEffect, useState, ReactNode } from "react"

type Props = {
    isOpen: Boolean,
    onClose: () => void
    children: ReactNode,
}

export function ModalDialog({ children, isOpen, onClose }: Props) {
    const [isVisible, setIsVisible] = useState(isOpen)
    const [noDisplay, setNoDisplay] = useState(!isOpen)

    useEffect(() => {
        let timer: NodeJS.Timeout
        if (!isOpen) {
            setIsVisible(false)
            timer = setTimeout(() => setNoDisplay(true), 200)
        } else {
            setNoDisplay(false)
            timer = setTimeout(() => setIsVisible(true), 50)
        }

        return () => { clearTimeout(timer) }
    }, [isOpen])

    if (noDisplay) {
        return null
    }

    return (
        <div
            className="fixed top-0 left-0 right-0 bottom-0 z-50 flex items-center justify-center"
        >
            <div
                className={classNames(
                    "fixed z-0 top-0 left-0 right-0 bottom-0 bg-black",
                    "transition-opacity duration-200",
                    { "opacity-20": isVisible },
                    { "opacity-0": !isVisible },
                )}
                onClick={onClose}
            />

            <div
                className={classNames(
                    "max-w-screen-md relative m-4 max-h-full overflow-auto",
                    "transition duration-200",
                    { "translate-y-0 ease-out": isVisible },
                    { "-translate-y-6 opacity-0 ease-in": !isVisible },
                )}
            >
                <div className="bg-gray100 shadow-xl rounded-lg p-8 text-on-light">
                    {children}
                </div>
            </div>
        </div>
    )
}

export default ModalDialog
