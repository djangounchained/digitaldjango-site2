import React from "react"
import dynamic from "next/dynamic"

function NoSsr({ children }) {
    // eslint-disable-next-line react/jsx-no-useless-fragment
    return <>{ children }</>
}

export default dynamic(() => Promise.resolve(NoSsr), {
    ssr: false,
})
