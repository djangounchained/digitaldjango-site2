import Image, { StaticImageData } from "next/image"
import React, { useState } from "react"
import ModalDialog from "./ModalDialog"

export type PortfolioImageThumbProps = {
    image: StaticImageData,
    description: string,
}

export function PortfolioImageThumb({
    image,
    description,
}: PortfolioImageThumbProps) {
    const [isOpen, setIsOpen] = useState(false)
    return (
        <div className="relative w-28 h-28">
            <button
                className="px-2 bg-gray-100 shadow-sm rounded-md overflow-hidden w-full h-full object-cover object-center"
                type="button"
                onClick={() => { setIsOpen(true) }}
            >
                <Image src={image} alt={description} className="w-full h-full object-cover object-center" />
            </button>
            <ModalDialog isOpen={isOpen} onClose={() => { setIsOpen(false) }}>
                <Image src={image} alt={description} />
            </ModalDialog>
        </div>
    )
}

export default PortfolioImageThumb
