import React from "react"
import Image from "next/image"
import { PortfolioItem } from "@/models/PortfolioItem"
import Link from "next/link"
import Typo from "./Typo"

type Props = {
    item: PortfolioItem,
}

export function PortfolioListItem({
    item,
}: Props) {
    return (
        <div className="relative md:my-12">
            <div className="container mx-auto px-4 flex items-center py-8">
                <div className="hidden md:flex md:flex-1 justify-center">
                    <Image
                        src={item.cover.src}
                        alt={item.cover.alt}
                        className="relative z-10 rounded-lg bg-white shadow object-cover object-top w-96 h-96"
                    />
                </div>
                <div className="md:flex-1 pb-8">
                    <Typo variant="h1" color="purple">{item.role}</Typo>
                    <Typo variant="h3" color="purple" className="mb-6">{item.title}</Typo>

                    <Typo className="mb-6" variant="body2">
                        {item.description.substring(0, 255)}...
                        <br />
                        <Link
                            href={`/portfolio/${item.id}`}
                            className="text-orange"
                            aria-label={`Read more about my work as ${item.role} at ${item.title}"`}
                        >
                            Read more
                        </Link>
                    </Typo>

                    <div className="flex flex-row">
                        <div className="flex-1">
                            <Typo variant="h5" color="red">When</Typo>
                            <Typo variant="body2">{item.startDate} - {item.endDate}</Typo>
                        </div>
                        <div className="flex-1">
                            <Typo variant="h5" color="red">What I did</Typo>
                            <Typo variant="body2">{item.role}</Typo>
                        </div>
                    </div>
                </div>
            </div>

            <div className="relative md:hidden">
                <div className="p-10 w-full">
                    <Image
                        src={item.cover.src}
                        alt={item.cover.alt}
                        className="relative z-10 rounded-lg bg-white shadow max-h-60 object-cover object-top"
                    />
                </div>
                <div className={`bg-${item.color} absolute bottom-0 right-0 left-0 h-1/2 z-0`} />
            </div>
        </div>
    )
}

export default PortfolioListItem
