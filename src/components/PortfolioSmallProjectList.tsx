import React from "react"
import Image from "next/image"
import { PortfolioItem } from "@/models/PortfolioItem"
import Link from "next/link"
import Typo from "./Typo"

type Props = {
    items: PortfolioItem[],
}

export function PortfolioSmallProjectList({
    items,
}: Props) {
    return (
        <div className="bg-purple py-8 md:py-16 w-screen">
            <ul className="flex flex-row overflow-y-auto w-screen gap-4 p-4 container max-w-screen-xl md:mx-auto">
                { items.map((item) => (
                    <li key={item.id} className="w-11/12 h-56 shrink-0 md:flex-1">
                        <Link href={`/portfolio/${item.id}`}>
                            <div className={`w-full h-full bg-${item.cover.background ?? "white"} 
                                shadow-xl rounded-lg relative overflow-hidden`}
                            >
                                <Image src={item.cover.src} alt={item.cover.alt} className="w-full h-full object-cover absolute z-0" />
                                <div className="absolute z-10 bottom-0 right-0 left-0 text-white p-2 bg-purple-50">
                                    <Typo variant="h2">
                                        {item.role}
                                    </Typo>
                                    <Typo variant="body2">
                                        <Typo component="span" variant="h6">{item.title}</Typo> - {item.startDate}
                                    </Typo>
                                </div>
                            </div>
                        </Link>
                    </li>
                ))}
            </ul>
        </div>
    )
}

export default PortfolioSmallProjectList
