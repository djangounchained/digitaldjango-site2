import React from "react"
import Typo from "./Typo"

type Props = {
    title: String,
}

export function PortfolioTag({
    title,
}: Props) {
    return (
        <div className="px-2 bg-gray-100 shadow-sm rounded-md">
            <Typo variant="body2">{title}</Typo>
        </div>
    )
}

export default PortfolioTag
