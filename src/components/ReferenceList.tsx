import React from "react"
import Image from "next/image"
import Link from "next/link"
import { Reference } from "@/models/Reference"
import Typo from "./Typo"

type Props = {
    items: Reference[],
}

export function ReferenceList({
    items,
}: Props) {
    return (
        <ul className="flex flex-row gap-12 p-4">
            { items.map((item) => (
                <li key={item.id} className="w-80 shrink-0">
                    <Link href={`/portfolio/${item.projectId}`}>
                        <div className="flex flex-col items-center">
                            <Image
                                src={item.photo.src}
                                alt={item.photo.alt}
                                className={`${item.photo.background ?? "bg-white"} w-32 h-32 
                                object-contain rounded-full shadow-lg p-2 mb-4`}
                            />

                            <Typo variant="body1" className="text-center italic mb-4">
                                &quot;{item.review}&quot;
                            </Typo>
                            <Typo variant="h3" className="text-center" color="orange">
                                {item.name}
                            </Typo>
                            <Typo variant="body2">
                                {item.role}
                            </Typo>

                        </div>
                    </Link>
                </li>
            ))}
        </ul>
    )
}

export default ReferenceList
