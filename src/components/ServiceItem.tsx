import React from "react"
import Typo from "./Typo"

type Props = {
    id: number
    title: string
    description: string
}

export function ServiceItem({
    id, title, description,
}: Props) {
    return (
        <div className="border-b border-orange-70">
            <Typo variant="h1">{id}</Typo>
            <Typo variant="h2" color="red" className="block md:h-24" gutterBottom>{title}</Typo>
            <Typo variant="body2" gutterBottom>{description}</Typo>
        </div>
    )
}

export default ServiceItem
