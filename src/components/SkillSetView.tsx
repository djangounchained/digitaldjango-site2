import { SkillSet } from "@/models/SkillSet"
import React from "react"
import Typo from "./Typo"

type Props = {
    skillSets: SkillSet[],
}

export function SkillsetView({
    skillSets,
}: Props) {
    return (
        <ul className="grid grid-cols-2 grid-flow-row gap-10 w-full pb-8">
            { skillSets.map((skillSet) => (
                <li key={skillSet.id} className="border-orange border-b pb-4">
                    <Typo variant="h2">{skillSet.category}</Typo>
                    <Typo variant="h3" color="red">{skillSet.amount}</Typo>

                    <ul className="">
                        { skillSet.skills.map((skill) => (
                            <li key={skill.id} className="flex flex-row">
                                <Typo variant="body2" className="flex-1">{skill.title}</Typo>
                                {skill.amount && (
                                    <Typo variant="body2">{skill.amount}</Typo>
                                )}
                            </li>
                        ))}
                    </ul>
                </li>
            ))}
        </ul>
    )
}

export default SkillsetView
