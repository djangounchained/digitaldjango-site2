import classNames from "classnames"
import React, { ReactNode } from "react"

const variants = {
    light: "bg-white text-purple",
    dark: "bg-purple text-white",
}

type Props = {
    variant?: keyof typeof variants,
    title?: ReactNode
    items?: [number, ReactNode][]
}

export function StickyMenu({
    variant = "dark",
    title = undefined,
    items = [],
}: Props) {
    return (
        <menu className={classNames(
            variants[variant],
            "sticky w-full top-0 z-40 p-2 h-14 flex flex-row justify-between items-center border-b border-b-orange-70",
        )}
        >
            {title}
            <ul className="flex flex-row gap-4">
                {items.map(([key, item]) => (
                    <li key={key}>
                        {item}
                    </li>
                ))}
            </ul>
        </menu>
    )
}

export default StickyMenu
