/* eslint-disable react/jsx-props-no-spreading */
import Link from "next/link"
import React, { MouseEventHandler, ReactNode } from "react"

type Props = {
    href: string
    external?: boolean
    onClick?: MouseEventHandler
    children?: ReactNode
}

export function TextLink({
    href,
    external = false,
    onClick,
    children,
}: Props) {
    return (
        <Link
            href={href}
            className="underline cursor-pointer hover:text-orange-70"
            target={external ? "_blank" : undefined}
            rel={external ? "noreferrer noreferrer" : undefined}
            onClick={onClick}
        >
            {children}
        </Link>
    )
}

export default TextLink
