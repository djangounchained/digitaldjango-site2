import classNames from "classnames"
import React, {
    ComponentType, ReactNode,
} from "react"

const variants = {
    h1: "text-xl leading-gl md:text-xxl md:leading-xl font-light font-title",
    h2: "text-lg leading-md md:text-xl md:leading-lg font-light font-title",
    h3: "text-lg leading-md font-light font-title",
    h4: "text-md lg:text-lg leading-none lg:leading-md font-normal lg:font-light font-body",
    h5: "text-md leading-none font-semibold font-body",
    h6: "text-sm leading-sm font-semibold font-body",
    body1: "text-md lg:text-md-desktop leading-none font-normal font-body",
    body2: "text-sm leading-none font-normal font-body",
}

const colors = {
    default: "",
    purple: "text-purple",
    orange: "text-orange",
    red: "text-red",
    black: "text-black",
    white: "text-white",
}

type Props<TAG extends keyof JSX.IntrinsicElements> = {
    component?: ComponentType | keyof JSX.IntrinsicElements
    variant?: keyof typeof variants
    color?: keyof typeof colors
    className?: string
    children?: ReactNode
    gutterBottom?: boolean
} & JSX.IntrinsicElements[TAG]

export function Typo<TAG extends keyof JSX.IntrinsicElements = "p">({
    component: TypoComponent = "p",
    variant = "body1",
    color = "default",
    className = undefined,
    gutterBottom = false,
    children,
}: Props<TAG>) {
    return (
        <TypoComponent
            className={classNames(
                variants[variant],
                colors[color],
                className,
                { "mb-4": gutterBottom },
            )}
        >
            {children}
        </TypoComponent>
    )
}

export default Typo
