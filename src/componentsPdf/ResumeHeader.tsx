import React from "react"
import {
    PDFViewer, Document, Page, View, Text, StyleSheet, Image,
} from "@react-pdf/renderer"
import { appStyles, colors } from "./appStyles"

const styles = StyleSheet.create({
    container: {
        height: "104",
        padding: "8",
        flexDirection: "row",
        backgroundColor: colors.purple.DEFAULT,
        color: colors.white,
        alignItems: "center",
    },
    image: {
        borderRadius: "100%",
        marginRight: "8",
    },
})

export function ResumeHeader() {
    return (
        <View style={styles.container}>
            <Image src="/media/django.jpg" style={styles.image} />

            <View style={{ flexGrow: 1, flexBasis: 0 }}>
                <Text style={[appStyles.h1, { color: colors.orange.DEFAULT }]}>Django Witmer</Text>
                <Text style={appStyles.h3}>Freelance Tech lead and senior software engineer</Text>
            </View>
            <View style={{}}>
                <Text style={appStyles.body2}>Netherlands</Text>
                <Text style={appStyles.body2}>Amsterdam</Text>
                <Text style={appStyles.body2}>+31 6 223 295 01</Text>
                <Text style={appStyles.body2}>django@digitaldjango.com</Text>
            </View>
        </View>
    )
}

export default ResumeHeader
