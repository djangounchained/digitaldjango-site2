import React, { ReactNode } from "react"
import {
    PDFViewer, Document, Page, View, Text, StyleSheet,
} from "@react-pdf/renderer"
import { PortfolioItem } from "@/models/PortfolioItem"
import { appStyles, colors } from "./appStyles"

const styles = StyleSheet.create({
    container: {},
})

export function ResumeHistoryItem({
    item,
}: { item: PortfolioItem }) {
    return (
        <View style={styles.container}>
            <Text style={appStyles.h4}>{item.role}</Text>
            <Text style={appStyles.h4}>{item.title}</Text>
            <Text style={appStyles.body2}>{item.startDate} - {item.endDate}</Text>
        </View>
    )
}

export default ResumeHistoryItem
