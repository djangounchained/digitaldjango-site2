import React, { ReactNode } from "react"
import {
    PDFViewer, Document, Page, View, Text, StyleSheet,
} from "@react-pdf/renderer"
import { appStyles, colors } from "./appStyles"

const styles = StyleSheet.create({
    container: {
        display: "flex",
        flexDirection: "row",
        flexGrow: 1,
    },

    titleContainer: {
        flexBasis: "104",
        flexGrow: 0,
        flexShrink: 0,
        marginBottom: "32",
    },

    title: {
        textAlign: "right",
        color: colors.orange.DEFAULT,
    },

    borderContainer: {
        flexDirection: "column",
        width: "24",
        alignItems: "center",
    },

    borderTop: {
        width: "1",
        flexBasis: 8,
        backgroundColor: colors.purple.DEFAULT,
    },

    border: {
        width: "1",
        flexGrow: 1,
        backgroundColor: colors.purple.DEFAULT,
    },

    borderIndicator: {
        width: "8",
        height: "8",
        borderRadius: "100%",
        backgroundColor: colors.purple.DEFAULT,
    },

    contentContainer: {
        marginBottom: "16",
        flexGrow: 1,
        flexBasis: 0,
    },
})

type Props = {
    title?: ReactNode,
    children: ReactNode,
}

export function ResumeItem({
    title,
    children,
}: Props) {
    return (
        <View style={styles.container}>
            <View style={styles.titleContainer}>
                <Text style={[appStyles.h2, styles.title]}>{title}</Text>
            </View>
            <View style={styles.borderContainer}>
                <View style={styles.borderTop} />
                <View style={styles.borderIndicator} />
                <View style={styles.border} />
            </View>
            <View style={styles.contentContainer}>
                {children}
            </View>
        </View>
    )
}

export default ResumeItem
