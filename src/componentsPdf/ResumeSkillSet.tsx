import React, { ReactNode } from "react"
import {
    PDFViewer, Document, Page, View, Text, StyleSheet,
} from "@react-pdf/renderer"
import { SkillSet } from "@/models/SkillSet"
import { skillset } from "@/portfolio/items"
import { appStyles, colors } from "./appStyles"

const styles = StyleSheet.create({
    container: {
        display: "flex",
        flexDirection: "row",
        flexGrow: 1,
        gap: 8,
    },

    item: {
        flexBasis: 0,
        flexGrow: 1,
    },
})

function Skill({ skillSet }: { skillSet: SkillSet }) {
    return (
        <View style={styles.item}>
            <Text style={appStyles.h3}>{skillSet.category}</Text>
            <Text style={appStyles.h4}>{skillSet.amount}</Text>
            { skillSet.skills.map((skill) => (
                <Text key={skill.id} style={appStyles.body2}>
                    {skill.title}
                </Text>
            ))}
        </View>
    )
}

export function ResumeSkillSet() {
    return (
        <View style={styles.container}>
            {skillset.map((skill) => (
                <Skill key={skill.id} skillSet={skill} />
            ))}
        </View>
    )
}

export default ResumeSkillSet
