import { StyleSheet, Font } from "@react-pdf/renderer"

Font.register({ family: "Montserrat", src: "/fonts/Montserrat-Light.ttf", fontWeight: "light" })
Font.register({ family: "Roboto", src: "/fonts/Roboto-Light.ttf", fontWeight: "light" })
Font.register({ family: "Roboto", src: "/fonts/Roboto-Regular.ttf", fontWeight: "normal" })

Font.registerHyphenationCallback((word) => [word])

export const colors = {
    transparent: "transparent",
    current: "currentColor",
    black: {
        DEFAULT: "#000000",
        50: "rgba(0, 0, 0, 0.5)",
    },
    white: "#ffffff",
    gray: {
        50: "#F3F3EC",
        100: "#e7e7d9",
    },
    orange: {
        DEFAULT: "#FA824C",
        70: "rgba(250, 131, 76, 0.7)",
    },
    purple: {
        DEFAULT: "#444054",
        50: "rgba(68, 64, 84, 0.5)",
    },
    red: "#FF6663",
}

export const appStyles = StyleSheet.create({
    h1: {
        fontSize: "18",
        fontFamily: "Montserrat",
        fontWeight: "light",
    },
    h2: {
        fontSize: "20pt",
        fontFamily: "Montserrat",
        fontWeight: "light",
    },
    h3: {
        fontSize: "13pt",
        fontFamily: "Montserrat",
        fontWeight: "light",
    },
    h4: {
        fontSize: "11pt",
    },
    body1: {
        fontSize: "11",
        fontFamily: "Roboto",
        fontWeight: "normal",
    },
    body2: {
        fontSize: "9",
        fontFamily: "Roboto",
        fontWeight: "normal",
    },
})
