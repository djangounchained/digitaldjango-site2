import { RefObject, useEffect, useState } from "react"

export function useParallax(ref: RefObject<HTMLElement>, fraction: number) {
    useEffect(() => {
        const updateParallaxElement = () => {
            const node = ref.current
            if (!node) { return }

            if (window.pageYOffset === 0) {
                node.style.top = "0px"
                return
            }

            if (window.pageYOffset > window.screen.height) {
                return
            }

            const currentOffset = node.offsetTop
            const newPos = Math.floor(currentOffset + ((Math.floor(window.pageYOffset / fraction) - currentOffset)))
            node.style.top = `${newPos}px`
        }
        window.addEventListener("scroll", updateParallaxElement)
        return () => window.removeEventListener("scroll", updateParallaxElement)
    }, [ref, fraction])
}

export function useIsInViewPort(ref: RefObject<Element>, threshold: number = 0.3) {
    const [intersecting, setIntersecting] = useState(false)

    useEffect(() => {
        const observer = new IntersectionObserver((entries) => {
            entries.forEach((element) => {
                if (ref.current && element.target === ref.current) {
                    setIntersecting(element.isIntersecting)
                }
            })
        }, { threshold })

        if (ref.current) {
            observer.observe(ref.current)
        }

        return () => {
            observer.disconnect()
        }
    }, [ref, threshold])

    return intersecting
}
