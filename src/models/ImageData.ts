import { StaticImageData } from "next/image"

export interface ImageData {
    src: StaticImageData
    alt: string
    background?: "bg-purple" | "bg-white" | "bg-orange" | "bg-red" | "bg-black"
}
