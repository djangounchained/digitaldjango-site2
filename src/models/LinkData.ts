export interface LinkData {
    href: string
    label: string
}
