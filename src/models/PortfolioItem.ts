import { ImageData } from "./ImageData"
import { LinkData } from "./LinkData"

export interface PortfolioItem {
    id: string
    title: string
    description: string
    color: string
    role: string
    startDate: string
    endDate: string
    tags: string[]
    links?: LinkData[]
    work?: string[]
    logo: ImageData
    cover: ImageData
    images: ImageData[]
}
