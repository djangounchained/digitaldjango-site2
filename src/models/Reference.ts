import { ImageData } from "./ImageData"

export interface Reference {
    id: string
    name: string
    photo: ImageData
    company: string
    role: string
    review: string
    phone: string
    email: string
    projectId: string
}
