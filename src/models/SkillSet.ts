import { Skill } from "./Skill"

export interface SkillSet {
    id: string,
    category: string,
    amount: string,
    skills: Skill[],
}
