import { PortfolioItem } from "./PortfolioItem"

export type TimelineItemMajor = {
    variant: "major"
    key: string
    portfolioItem: PortfolioItem
}

export type TimelineItemMinorProjects = {
    variant: "minorProjects"
    key: string
    items: PortfolioItem[]
}

export type TimelineItem = | TimelineItemMajor | TimelineItemMinorProjects
