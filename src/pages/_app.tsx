import React from "react"
import "@/styles/globals.css"
import type { AppProps } from "next/app"

require("typeface-montserrat")
require("typeface-roboto")

export default function App({ Component, pageProps }: AppProps) {
    // eslint-disable-next-line react/jsx-props-no-spreading
    return <Component {...pageProps} />
}
