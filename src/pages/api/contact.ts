import type { NextApiRequest, NextApiResponse } from "next"
import nodemailer from "nodemailer"

type Data = {
    error?: string,
    success?: string,
}

type Body = {
    name?: string,
    email?: string,
    message?: string,
}

function buildEmailBody(name: string, email: string, text: string): string {
    return `----------------\n\
name : ${name}\n\
email : ${email}\n\
----------------\n\
${text}`
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Data>,
) {
    const data = req.body as Body
    if (!data.name || data.name.length < 2) {
        res.status(400).json({ error: "Please tell me your name." })
        return
    }
    if (!data.email || data.email.length < 3) {
        res.status(400).json({ error: "Please provide a valid E-mail address." })
        return
    }
    if (!data.message || data.message.length < 2) {
        res.status(400).json({ error: "Message is required." })
        return
    }

    const transporter = nodemailer.createTransport({
        host: "smtp.transip.email",
        secure: true,
        port: 465,
        auth: {
            user: process.env.MAIL_SENDER_USERNAME,
            pass: process.env.MAIL_SENDER_PASSWORD,
        },
    })

    const mailOptions = {
        from: "machine@digitaldjango.com",
        to: "django@digitaldjango.com",
        replyTo: `${data.name}<${data.email}>`,
        subject: "DigitalDjango contact form",
        text: buildEmailBody(data.name, data.email, data.message),
    }

    if (req.method === "POST") {
        try {
            const info = await transporter.sendMail(mailOptions)

            res.status(250).json({
                success: `Message delivered to ${info.accepted}`,
            })
        } catch (e: any) {
            res.status(500).json({
                error: e.toString(),
            })
        }
    } else {
        res.status(404).json({
            error: "Not found",
        })
    }
}
