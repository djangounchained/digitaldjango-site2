/* eslint-disable max-len */
import React, { MouseEventHandler, ReactNode } from "react"
import Typo from "@/components/Typo"
import PortfolioItem from "@/components/PortfolioListItem"
import {
    homeTimeline, references, skillset,
} from "@/portfolio/items"
import StickyMenu from "@/components/StickyMenu"
import Link from "next/link"
import MenuButton from "@/components/MenuButton"
import HomeHeader from "@/components/HomeHeader"
import Button from "@/components/Button"
import ContactForm from "@/components/ContactForm"
import ServiceItem from "@/components/ServiceItem"
import Logo from "@/components/Logo"
import { TimelineItem } from "@/models/TimelineItem"
import PortfolioSmallProjectList from "@/components/PortfolioSmallProjectList"
import SkillsetView from "@/components/SkillSetView"
import ReferenceList from "@/components/ReferenceList"
import Head from "next/head"

function smoothScroll(id: string): MouseEventHandler {
    return (e) => {
        e.preventDefault()
        const el = document.getElementById(id)
        if (el) {
            el.scrollIntoView({ behavior: "smooth", block: "start" })
        }
    }
}

function renderPortfolioItem(item: TimelineItem): ReactNode {
    switch (item.variant) {
    case "major":
        return (<PortfolioItem item={item.portfolioItem} />)
    case "minorProjects":
        return (<PortfolioSmallProjectList items={item.items} />)
    default:
        return undefined
    }
}

export default function Home() {
    return (
        <main className="flex min-h-screen flex-col items-center justify-between">
            <Head>
                <title>Django, tech lead and software engineer portfolio</title>
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <meta name="robots" content="index, follow" />
                <meta
                    name="description"
                    content="Accomplished tech lead and senior software engineer with a proven track record of leading teams and delivering high-quality solutions."
                    key="desc"
                />
                <meta
                    property="og:description"
                    content="Accomplished tech lead and senior software engineer with a proven track record of leading teams and delivering high-quality solutions."
                />
                {/* <meta
          property="og:image"
          content="https://example.com/images/cool-page.jpg"
        /> */}
            </Head>
            <HomeHeader>
                <div className="container mx-auto max-w-xl">
                    <Typo variant="h1" color="orange" gutterBottom>Django Witmer</Typo>
                    <Typo gutterBottom variant="h4">
                        Accomplished tech lead and senior software engineer from Amsterdam with a proven track record of leading
                        teams and delivering high-quality solutions to companies ranging from small startups to large institutions.
                    </Typo>
                    <Button onClick={smoothScroll("about")}><Typo>Read more</Typo>
                    </Button>
                </div>
                {/* <div className="absolute bottom-0 right-0 left-0 z-10">
                    <ul className="grid grid-cols-3 grid-flow-row gap-4 container mx-auto max-w-xl">
                        <li>
                            <Typo variant="body2">
                                Engineer since 2007 with
                            </Typo>
                        </li>
                        <li>
                            <TextLink href="#portfolio" onClick={smoothScroll("portfolio")}>
                                <Typo variant="body2">
                                    {allPortfolioItems.length} Projects finished
                                </Typo>
                            </TextLink>
                        </li>
                        {skillset.map((skill) => (
                            <li key={skill.id}>
                                <TextLink href="#skillset" onClick={smoothScroll("skillset")}>
                                    <Typo variant="body2">{skill.category}: {skill.amount}</Typo>
                                </TextLink>
                            </li>
                        ))}
                    </ul>
                </div> */}
            </HomeHeader>

            <StickyMenu
                title={(<Link href="/" aria-label="Home"><Logo size={40} /></Link>)}
                items={[
                    [1, (<Link href="#about" onClick={smoothScroll("about")}><MenuButton>About</MenuButton></Link>)],
                    [2, (<Link href="#portfolio" onClick={smoothScroll("portfolio")}><MenuButton>Portfolio</MenuButton></Link>)],
                    [3, (<Link href="#contact" onClick={smoothScroll("contact")}><MenuButton>Contact</MenuButton></Link>)],
                ]}
            />

            {/* <a id="about">About</a> */}
            {/* <section className="w-full bg-purple text-white overflow-hidden py-8" id="about">
                <div className="container mx-auto px-4 max-w-2xl">
                    <Typo
                        variant="h2"
                        color="orange"
                        component="h1"
                        className="text-center py-10"
                    >
                        About me
                    </Typo>
                    <AnimateIn transition="fade">
                        <Typo gutterBottom>
                            Throughout my career, I have consistently proven myself as a versatile and proactive lead software engineer with an unyielding dedication to excellence. I possess a deep passion for coaching and mentoring individuals and teams, which has allowed me to cultivate an extensive skill set in team building, communication, and problem-solving. My experience in software architecture is vast, as I have designed and delivered multiple large-scale software solutions across various domains.
                        </Typo>
                        <Typo gutterBottom>
                            As a lifelong learner, I take pride in my ability to stay up-to-date with emerging technologies and identify their potential applications. I am well-versed in numerous programming languages and have experience working with a wide range of tools and frameworks. My track record of delivering innovative and efficient solutions to complex problems is a testament to my strong analytical skills and ability to think outside the box.
                        </Typo>
                        <Typo gutterBottom>
                            Apart from my technical skills, I am also a natural leader with over five years of experience in management, coaching, and team training. I founded my own <TextLink href="https://www.venturecoders.nl/" external>programming course platform</TextLink>, which has been successful in empowering underprivileged youth with essential coding skills. I believe that it is important to use my technical expertise to make a positive impact on the community and strive to do so in every aspect of my work.
                        </Typo>
                        <Typo gutterBottom>
                            With my extensive experience and passion for learning, I am capable of mastering new technologies, programming languages, and tech stacks with ease. As a self-taught professional, I have developed a strong ability to solve any type of technical problem. Feel free to contact me to discuss how I can assist with your project needs.
                        </Typo>

                        <div className="flex justify-center">
                            <Button onClick={smoothScroll("contact")}>Get in contact now</Button>
                        </div>
                    </AnimateIn>
                </div>
            </section> */}

            {/* <Typo gutterBottom>
                            I am an experienced lead software engineer with a passion for coaching and mentoring individuals and teams, as well as a strong background in software architecture. With my ability to quickly adapt to new technologies and dedication to quality, I strive to deliver innovative and efficient solutions to complex problems.
                        </Typo>
                        <Typo gutterBottom>
                            In addition to my technical background, I am also a leader with 5 years of experience in management, coaching, and team training. I have founded my own successful <TextLink href="https://www.venturecoders.nl/" external>programming course platform</TextLink> to empower underprivileged youth with essential coding skills.
                        </Typo> */}

            <section id="about">
                <div className="container mx-auto pb-16 px-6">
                    <Typo
                        variant="h1"
                        color="orange"
                        component="h1"
                        className="pt-14 pb-8"
                    >
                        Expert technical leadership and software engineering services.<br />
                        {/* <Link href="#portfolio" onClick={smoothScroll("portfolio")} className="underline">View portfolio</Link> */}
                    </Typo>

                    <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-4 grid-flow-row gap-8">
                        <ServiceItem
                            id={1}
                            title="Interim Tech lead or IT advisor"
                            description="As a temporary CTO I can provide technical leadership, guidance and continuity to companies during transition periods or when there is a leadership gap. I can also coach, support and advice the lead engineer or founder with technical guidance."
                        />

                        <ServiceItem
                            id={2}
                            title="Lead Software Engineer"
                            description="As a skilled and experienced technical leader with expertise in various technologies, I bring a user-centered mindset, a passion for innovation, and a commitment to quality to any team, helping to drive successful project completion and achieve business goals. As well as hands on development with various front and backend technologies."
                        />
                        <ServiceItem
                            id={3}
                            title="Coach and Teacher"
                            description="As an experienced coach and teacher with a passion for software engineering, I inspire individuals to achieve their full potential, providing guidance, support, and constructive feedback to help develop new skills and achieve goals."
                        />

                        <ServiceItem
                            id={4}
                            title="Software Architect"
                            description="As a software architect, I design and develop complex software systems with scalability, security, and user experience in mind. My deep understanding of design patterns, development methodologies, and advanced technologies, coupled with excellent communication skills, enable me to effectively collaborate with teams and drive successful outcomes for the business."
                        />
                    </div>
                </div>
            </section>

            <section className="w-full bg-purple text-white" id="skillset">
                <div className="container mx-auto px-6 py-12">
                    <div className="flex flex-col md:flex-row gap-8 lg:gap-24 items-center">

                        <div className="flex-1 md:order-2">
                            <Typo
                                variant="h2"
                                color="orange"
                                component="h1"
                                className="pb-4 text-center"
                            >
                                Full stack
                            </Typo>
                            <SkillsetView skillSets={skillset} />
                        </div>

                        <div className="flex-1">
                            <div className="bg-white text-black shadow rounded-lg p-8">
                                <Typo
                                    variant="h2"
                                    color="orange"
                                    component="h1"
                                    className="pb-8 mx-auto text-center max-w-xs italic"
                                >
                                    &quot;Quickly master any new programming language or tech stack&quot;
                                </Typo>
                                <Typo className="pb-16">
                                    I possess a strong ability to quickly grasp and master new technologies, programming languages, and tech stacks, making me a natural autodidact. All my knowledge and skills have been self-taught, and I enjoy the challenge of exploring and learning new things in the field of software development.<br />
                                    <br />
                                    This allows me to solve a wide range of technical problems. Feel free to contact me to discuss how I can assist with your project needs.
                                </Typo>
                                <div className="flex justify-center">
                                    <Button onClick={smoothScroll("contact")}>Get in contact now</Button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section id="references">
                <Typo
                    variant="h2"
                    color="orange"
                    component="h1"
                    className="text-center py-10"
                >
                    References
                </Typo>
                <div className="px-6 pb-12 container overflow-x-auto">

                    <ReferenceList items={references} />
                </div>
            </section>

            <section className="w-full bg-purple text-white pb-10" id="contact">
                <Typo
                    variant="h2"
                    color="orange"
                    component="h1"
                    className="text-center py-10"
                >
                    Let&apos;s get in touch
                </Typo>
                <ContactForm />
            </section>

            <section id="portfolio">
                <Typo
                    variant="h2"
                    color="orange"
                    component="h1"
                    className="text-center py-10"
                >
                    Portfolio
                </Typo>

                <ul>
                    {homeTimeline.map((item) => (
                        <li key={item.key}>
                            { renderPortfolioItem(item) }
                        </li>
                    ))}
                </ul>
            </section>
        </main>
    )
}
