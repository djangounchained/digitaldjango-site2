import React from "react"
import Image from "next/image"
import { PortfolioItem } from "@/models/PortfolioItem"
import { allPortfolioItems } from "@/portfolio/items"
import { ParsedUrlQuery } from "querystring"
import { GetStaticProps } from "next"
import StickyMenu from "@/components/StickyMenu"
import Typo from "@/components/Typo"
import { useRouter } from "next/router"
import classNames from "classnames"
import PortfolioTag from "@/components/PortfolioTag"
import Link from "next/link"
import Head from "next/head"

type Props = {
    item: PortfolioItem,
}

export default function PortfolioPage({ item }: Props) {
    const router = useRouter()

    return (
        <main className="min-h-screen">
            <Head>
                <title>{item.role} at {item.title}</title>
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <meta name="robots" content="index, follow" />
                <meta
                    name="description"
                    content={`${item.description.substring(0, 160)}...`}
                    key="desc"
                />
                <meta
                    property="og:description"
                    content={`${item.description.substring(0, 160)}...`}
                />
            </Head>
            <StickyMenu
                variant="light"
                title={(
                    <Typo variant="h3" component="h1">Portfolio</Typo>
                )}
                items={[
                    [1, (
                        <button type="button" onClick={() => { router.back() }}>
                            <Typo variant="body1" color="red" className="hover:underline">
                                Back
                            </Typo>
                        </button>
                    )],
                ]}
            />

            <section className="container mx-auto p-4 md:mt-20 max-w-4xl">
                <Image
                    src={item.logo.src}
                    alt={item.logo.alt}
                    className={classNames(
                        item.logo.background ? item.logo.background : "bg-white",
                        "shadow rounded-xl w-24 h-24 md:w-40 md:h-40 object-contain p-4 float-left mr-4 mb-4",
                    )}
                />
                <Typo variant="h1" className="mb-4">{item.role}</Typo>
                <Typo variant="h3" className="mb-4">{item.title}</Typo>
                {item.links && (
                    <ul className="flex flex-row flex-wrap gap-4 mb-8">
                        {item.links.map(({ href, label }) => (
                            <li key={href}>
                                <Link href={href} target="_blank" rel="noopener noreferrer">
                                    <Typo variant="body2" className="underline text-orange">{label}</Typo>
                                </Link>
                            </li>
                        ))}
                    </ul>
                )}
                <Typo className="whitespace-pre-wrap">{item.description}</Typo>

                {item.work && (
                    <>
                        <Typo variant="h3" className="mt-4">My responsibilities</Typo>
                        <ul className="list-disc px-8 py-4">
                            {item.work.map((workItem) => (
                                <li key={workItem}><Typo variant="body2">{workItem}</Typo></li>
                            ))}
                        </ul>
                    </>
                )}
            </section>

            <section className="container mx-auto p-4 max-w-4xl">
                {item.tags && (
                    <ul className="flex flex-row flex-wrap gap-4 mb-10">
                        {item.tags.map((tag) => (
                            <li key={tag}><PortfolioTag title={tag} /></li>
                        ))}
                    </ul>
                )}
                <div className="flex flex-row">
                    <div className="flex-1">
                        <Typo variant="h5" color="red">Start</Typo>
                        <Typo variant="body2">{item.startDate}</Typo>
                    </div>
                    <div className="flex-1">
                        <Typo variant="h5" color="red">End</Typo>
                        <Typo variant="body2">{item.endDate}</Typo>
                    </div>
                </div>
            </section>

            <section className="container mx-auto p-4 max-w-4xl">
                <ul className="flex flex-row flex-wrap items-center justify-center gap-4">
                    {item.images.map(({ src, alt }) => (
                        <li key={alt}>
                            <Image src={src} alt={alt} />
                        </li>
                    ))}
                </ul>

            </section>
        </main>
    )
}

export async function getStaticPaths() {
    return {
        paths: allPortfolioItems.map((item) => ({
            params: { id: item.id },
        })),
        fallback: false,
    }
}

interface IParams extends ParsedUrlQuery {
    id: string
}

export const getStaticProps: GetStaticProps = async (context) => {
    const { id } = context.params as IParams
    const item = allPortfolioItems.find((i) => i.id === id)
    return {
        props: {
            item,
        },
    }
}
