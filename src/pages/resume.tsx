/* eslint-disable max-len */
import React from "react"
import {
    PDFViewer, Document, Page, View, Text, StyleSheet,
} from "@react-pdf/renderer"
import NoSsr from "@/components/NoSsr"
import { allPortfolioItems } from "@/portfolio/items"
import ResumeHeader from "@/componentsPdf/ResumeHeader"
import ResumeItem from "@/componentsPdf/ResumeItem"
import { appStyles } from "@/componentsPdf/appStyles"
import SkillsetView from "@/components/SkillSetView"
import ResumeSkillSet from "@/componentsPdf/ResumeSkillSet"
import ResumeHistoryItem from "@/componentsPdf/ResumeHistoryItem"

const styles = StyleSheet.create({
    container: {
        padding: "8",
    },
})

export default function Resume() {
    return (
        <NoSsr>
            <PDFViewer className="w-screen h-screen">
                <Document>
                    <Page size="A4">
                        <ResumeHeader />

                        <View style={styles.container}>
                            <ResumeItem title="Summary">
                                <Text style={appStyles.body1}>
                                    As co-founder and teacher at VentureCoders, I was responsible for developing educational materials and a course platform as well as giving lectures, classes and talks about software engineering. We offer beginner courses in creating Android apps as well as advanced courses tailored for businesses.
                                </Text>
                                <Text style={appStyles.body1}>
                                    As co-founder and teacher at VentureCoders, I was responsible for developing educational materials and a course platform as well as giving lectures, classes and talks about software engineering. We offer beginner courses in creating Android apps as well as advanced courses tailored for businesses.
                                </Text>
                            </ResumeItem>
                            <ResumeItem title="Full-stack">
                                <ResumeSkillSet />
                                <Text style={appStyles.body2}>
                                    In addition, I can quickly master any new programming language or tech stack. I enjoy the challenge of exploring and learning new things. This allows me to solve a wide range of technical problems.
                                </Text>
                            </ResumeItem>
                            {allPortfolioItems.map((item, index) => (
                                <ResumeItem title={index === 0 ? "Latest projects" : ""}>
                                    <ResumeHistoryItem item={item} />
                                </ResumeItem>
                            ))}

                            <ResumeItem title="References">
                                <Text style={appStyles.body2}>
                                    Show references
                                </Text>
                            </ResumeItem>

                            <ResumeItem title="Self taught">
                                <Text style={appStyles.body2}>
                                    Explain here
                                </Text>
                            </ResumeItem>
                        </View>

                        {/* <View style={{ padding: "16pt" }}>
                            {allPortfolioItems.map((item) => (
                                <View key={item.id} style={{ paddingBottom: "8pt" }}>
                                    <Text style={styles.h3}>{item.role}</Text>
                                    <Text style={styles.body2}>{item.title} ({item.startDate} - {item.endDate})</Text>
                                    <Text style={styles.body2}>{item.description}</Text>
                                </View>
                            ))}
                        </View> */}
                    </Page>
                </Document>
            </PDFViewer>
        </NoSsr>
    )
}
