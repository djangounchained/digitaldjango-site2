/* eslint-disable max-len */
import { PortfolioItem } from "@/models/PortfolioItem"

import logo from "./logo.jpg"
import screenshot1 from "./screenshot1.jpg"
import screenshot2 from "./screenshot2.png"
import screenshot3 from "./screenshot3.jpg"

// eslint-disable-next-line quotes
const name = "Appical"
const description = `As Engineering Lead for an Appical, I was tasked with creating an app to streamline the process of onboarding new employees. I led my team to build a mobile app using native Kotlin and Swift, which was a great success and was completed in under four months. This app enabled new employees to easily navigate the onboarding process and get up to speed quickly.

After the mobile app was completed, I was then tasked with updating the organization's web app. I took on this challenge head-on and learned React, a popular JavaScript library for building user interfaces. I recruited a new team and together we completed the project in under four months as well. This allowed for a seamless transition between the mobile and web apps, improving the overall user experience.

During both projects, I was responsible for defining and implementing the architecture on Android, iOS, and React platforms. I also defined and implemented workflows, researched and implemented new technologies, and translated user and business requirements into technical design. As a leader, I coached and mentored my team of ten people, providing guidance and support throughout the development process.

Through my dedication to quality and my ability to lead and motivate my team, I was able to successfully complete both projects on time and within budget. I am proud of the work we accomplished and am always looking for new challenges to tackle in the ever-changing world of software development.`

export const appical: PortfolioItem = {
    id: "appical",
    title: name,
    description,
    color: "purple",
    role: "Engineering lead",
    startDate: "November 2016",
    endDate: "December 2018",
    tags: ["Lead developer", "iOS", "Swift", "Android", "Kotlin", "Architecture", "Coaching", "Push", "Realm db", "Javascript", "React", "Circle CI"],
    links: [
        { href: "https://www.appical.net", label: "Website" },
    ],
    work: [
        "Developing the technical design and architecture for the Appical app on various platforms (iOS, Android, and Web)",
        "Developing the Android app (Kotlin)",
        "Developing the iOS app (Swift)",
        "Managing the Front-end team",
        "Developing and managing the Web app (React)",
        "Managing the backend team (C# .NET, MySQL)",
        "Guaranteeing a 99% crash-free rate",
        "Developing reactive architecture with RX",
        "Promoting and encouraging the TTD mentality in the team",
        "Streamlining communication with other teams",
        "Selecting new team members for the development team",
        "Coaching and training the development team",
        "Providing input and advice on future developments (technical, functional, and team-oriented)",
        "Translating user requirements into technical designs",
        "Setting up continuous integration and automatic releases with circleCI",
    ],
    logo: {
        src: logo,
        alt: `${name} logo`,
        background: "bg-purple",
    },
    cover: {
        src: screenshot1,
        alt: `${name} example image`,
    },
    images: [
        {
            alt: `${name} example image`,
            src: screenshot1,
        },
        {
            alt: `${name} screenshots`,
            src: screenshot2,
        },
        {
            alt: `${name} more screenshots`,
            src: screenshot3,
        },
    ],
}
