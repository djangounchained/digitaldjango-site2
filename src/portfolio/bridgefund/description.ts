/* eslint-disable max-len */
import { PortfolioItem } from "@/models/PortfolioItem"

import logo from "./logo.png"
import screenshot1 from "./screenshot1.png"

// eslint-disable-next-line quotes
const name = "Bridgefund"
const description = `BridgeFund is a platform that aims to bridge the gap between entrepreneurs and their financial needs, without the intermediation of traditional banks. Their innovative approach to financing provides a streamlined process that empowers businesses to grow and succeed.

At DigitalDjango, we are proud to have played a crucial role in the success of BridgeFund. We have taken on the full development of the BridgeFund apps for both Android and iOS, providing cutting-edge technology that enhances the user experience and ensures seamless functionality. Me and my team of expert developers is committed to delivering outstanding results that meet the unique needs of each client, and I am thrilled to have been part of this project.`

export const bridgefund: PortfolioItem = {
    id: "bridgefund",
    title: name,
    description,
    color: "purple",
    role: "App development",
    startDate: "August 2021",
    endDate: "Now",
    tags: ["Lead developer", "iOS", "Swift", "Android", "Kotlin", "Architecture", "Coaching"],
    links: [
        { href: "https://www.bridgefund.nl/", label: "Website" },
        { href: "https://apps.apple.com/nl/app/bridgefund/id1596915877", label: "App store" },
    ],
    work: [
        "Development and maintenance of a brand new iOS app",
        "Development and maintenance of a brand new Android app",
        "Development of technical design and architecture focused on long-term quality",
        "Management of backend team in Sri Lanka",
        "Streamlining collaboration with the QA team",
        "Providing input and advice on future developments (both technical, functional, and team-related)",
        "Supervision and coaching of 2 junior engineers",
        "Improving collaboration with other teams",
    ],
    logo: {
        src: logo,
        alt: `${name} logo`,
        background: "bg-purple",
    },
    cover: {
        src: screenshot1,
        alt: `${name} example image`,
    },
    images: [
        {
            alt: `${name} example image`,
            src: screenshot1,
        },
    ],
}
