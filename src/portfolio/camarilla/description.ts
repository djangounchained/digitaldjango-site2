/* eslint-disable max-len */
import { PortfolioItem } from "@/models/PortfolioItem"

import logo from "./logo.jpg"

import screenshot1 from "./screenshot1.jpg"

// eslint-disable-next-line quotes
const name = "Camarilla"
const description = "At Camarilla, I took on the role of Lead Android Engineer to oversee a team of three. Upon joining the company, I found the existing code in a poor state and prioritized refactoring efforts while maintaining feature development. Through my efforts, I was able to improve developer efficiency and productivity by 50%. I also provided guidance to the iOS team, ensuring consistency in the development process. Despite the ultimate failure of the social media startup, I am proud of the work my team and I accomplished to improve the codebase and contribute to the overall success of the platform."

export const camarilla: PortfolioItem = {
    id: "camarilla",
    title: name,
    description,
    color: "purple",
    role: "Xamarin engineer",
    startDate: "June 2016",
    endDate: "March 2017",
    tags: ["Lead developer", "Android", "C#", "Xamarin", "Android", "Coaching"],
    work: [],
    logo: {
        src: logo,
        alt: `${name} logo`,
    },
    cover: {
        src: screenshot1,
        alt: `${name} example image`,
    },
    images: [
        {
            alt: `${name} screenshot 1`,
            src: screenshot1,
        },
    ],
}
