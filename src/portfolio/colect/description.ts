/* eslint-disable max-len */
import { PortfolioItem } from "@/models/PortfolioItem"

import logo from "./logo.png"
import screenshot1 from "./screenshot1.jpg"
import screenshot2 from "./screenshot2.jpg"
import screenshot3 from "./screenshot3.jpg"

// eslint-disable-next-line quotes
const name = "Colect"
const description = `As interim tech lead, I was responsible for maintaining and further developing the highly complex Colect app (Objective-C) during a transitional period when the company was searching for a new CTO. In addition, I developed a completely new version of the showroom application (Swift). As a team lead, I also had the responsibility of remotely onboarding and guiding new iOS developers based in Ukraine.

During my time as tech lead, I worked diligently to ensure that the Colect app remained stable and functional while the company underwent a significant leadership change. I was also tasked with developing a new version of the showroom application using Swift, which involved significant planning, coding, and testing to ensure a seamless transition for users.

As the team lead, I took on the additional responsibility of overseeing the remote onboarding and training of new iOS developers. This involved clear communication, setting up development environments, and providing guidance and mentorship to ensure that they were able to quickly become productive members of the team.

Overall, my experience as the lead iOS developer was challenging and rewarding, and I am proud of the work that I accomplished during this transitional phase for the company.`

export const colect: PortfolioItem = {
    id: "colect",
    title: name,
    description,
    color: "purple",
    role: "Tech lead A.I.",
    startDate: "March 2021",
    endDate: "June 2021",
    tags: ["Lead developer", "CTO A.i", "iOS", "Swift", "Objective-C", "Architecture", "Coaching"],
    links: [
        { href: "https://www.colect.io/", label: "Website" },
    ],
    work: [
        "Continued development and improving stability",
        "Developing technical design and architecture aimed at long-term quality for the showroom application",
        "Management and maintenance of a very large and complex legacy codebase",
        "Establishing a development workflow including code reviews, QA, and testing with the goal of increasing app quality and scaling teams in the future",
        "Streamlining collaboration with the QA team",
        "Coaching and training the development team",
        "Providing input and advice on future developments (technical, functional, and team-related)",
        "Direct communication and feedback with the support team and clients",
    ],
    logo: {
        src: logo,
        alt: `${name} logo`,
        background: "bg-black",
    },
    cover: {
        src: screenshot2,
        alt: "The Colect showroom",
    },
    images: [
        {
            alt: `${name} example`,
            src: screenshot1,
        },
        {
            alt: `${name} showroom`,
            src: screenshot2,
        },
        {
            alt: `${name} application`,
            src: screenshot3,
        },
    ],
}
