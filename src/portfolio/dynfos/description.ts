/* eslint-disable max-len */
import { PortfolioItem } from "@/models/PortfolioItem"

import logo from "./logo.png"
import screenshot1 from "./screenshot1.jpg"

// eslint-disable-next-line quotes
const name = "Dynfos"
const description = `As Android developer, I led the development of an Android application to replace an existing Windows CE application for internal logistics operations. This project required a complete rewrite as the windows CE hardware has become unavailable. My expertise in app architecture was crucial to its success.

Additionally, my deep understanding of SQL was essential, as the system is very database intensive. Through my leadership and technical skills, we were able to develop a new application that improved productivity and accuracy for the logistics team. The final product was a user-friendly application that utilized modern technology to streamline the logistics process.`

export const dynfos: PortfolioItem = {
    id: "dynfos",
    title: name,
    description,
    color: "red",
    role: "App development",
    startDate: "September 2022",
    endDate: "Now",
    tags: ["Lead developer", "Android", "Kotlin", "Architecture", "Coaching"],
    links: [
        { href: "https://www.dynfos.nl/", label: "Website" },
    ],
    work: [
        "Development of a new Android App for time tracking",
        "Development of a new App to support logistics",
        "Development of technical design and architecture focused on long-term quality",
        "Streamlining collaboration with the QA team",
        "Providing input and advice on future development",
    ],
    logo: {
        src: logo,
        alt: `${name} logo`,
    },
    cover: {
        src: screenshot1,
        alt: "Dynfos screenshots",
    },
    images: [
        {
            alt: `${name} screenshots`,
            src: screenshot1,
        },
    ],
}
