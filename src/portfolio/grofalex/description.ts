/* eslint-disable max-len */
import { PortfolioItem } from "@/models/PortfolioItem"

import logo from "./logo.png"
import screenshot1 from "./screenshot1.jpg"
import screenshot2 from "./screenshot2.png"
import screenshot3 from "./screenshot3.png"

// eslint-disable-next-line quotes
const name = "Grofalex"
const description = `In collaboration with the client, I developed a planning tool for scheduling workers on a large scale for ecological fieldwork. As the lead for this project, my responsibilities included developing the concept to a workable functional and technical design, UX design, architecture, and project management. The programming work was outsourced to a team of developers. Despite the tight deadline, we were able to complete the entire project in just three months.

The tech stack included React for the frontend, Node.js for the backend, and Postgres for the database. We utilized Prisma as the ORM for database management, which enabled efficient data modeling and synchronization. As a result, the client was able to use the planning tool to schedule students more effectively, which resulted in a more efficient ecological fieldwork program.`

export const grofalex: PortfolioItem = {
    id: "grofalex",
    title: name,
    description,
    color: "orange",
    role: "Concept development",
    startDate: "November 2022",
    endDate: "Now",
    tags: ["Lead engineer", "React", "NodeJS", "Javascript", "Postgres", "Prisma"],
    links: [
        { href: "http://grofalex.nl/", label: "Website" },
        { href: "http://app.grofalex.nl/", label: "Application" },
    ],
    work: [
        "Developing the idea to a functional and technical design",
        "Development of UI/UX",
        "Development of technical design and architecture focused on long-term quality",
        "Stack: React, NodeJS, Postgres, Prisma, Vercel, AWS",
        "Managing the engineering team",
        "Assistance in writing complex SQL queries",
        "Project management",
        "Providing advice and recommendations on future developments",
    ],
    logo: {
        src: logo,
        alt: `${name} logo`,
    },
    cover: {
        src: screenshot1,
        alt: `${name} screenshot`,
    },
    images: [
        {
            alt: `${name} screenshot`,
            src: screenshot1,
        },
        {
            alt: `${name} screenshot 1`,
            src: screenshot2,
        },
        {
            alt: `${name} screenshot 2`,
            src: screenshot3,
        },
    ],
}
