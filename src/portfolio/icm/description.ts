/* eslint-disable max-len */
import { PortfolioItem } from "@/models/PortfolioItem"

import logo from "./logo.png"
import cover from "./cover.png"

const name = "ICM group"
// eslint-disable-next-line quotes
const description = `During my internship as a web developer, I worked with PHP and JavaScript to develop and maintain web applications. I also gained experience with .NET framework. My responsibilities included writing clean, efficient code, troubleshooting technical issues, and collaborating with team members to ensure successful project completion. Throughout my internship, I demonstrated problem-solving skills and a willingness to learn and adapt to new technologies.`

export const icm: PortfolioItem = {
    id: "icm",
    title: name,
    description,
    color: "orange",
    role: "Intern software engineer",
    startDate: "2007",
    endDate: "2009",
    tags: ["PHP", "Javascript", ".NET"],
    work: [
        "Development of websites for clients",
    ],
    links: [
        { href: "https://icmgroep.nl/", label: "Website" },
    ],
    logo: {
        src: logo,
        alt: `${name} logo`,
    },
    cover: {
        src: cover,
        alt: `${name} cover`,
    },
    images: [],
}
