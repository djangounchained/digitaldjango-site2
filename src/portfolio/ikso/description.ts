/* eslint-disable max-len */
import { PortfolioItem } from "@/models/PortfolioItem"

import logo from "./logo.png"
import cover from "./cover.jpg"
import screenshot1 from "./screenshot1.jpg"
import screenshot2 from "./screenshot2.png"
import screenshot3 from "./screenshot3.png"

// eslint-disable-next-line quotes
const description = `The growth and development platform that I built from the ground up is an innovative and comprehensive solution that helps people in a team give each other open and honest feedback to promote growth and development. As the only developer, I was fully responsible for the platform's development and maintenance, and I played a key role in each of the four iterations of the platform.

Through my meticulous approach to development and my passion for creating high-quality solutions, I was able to deliver a platform that is not only user-friendly but also technically advanced and scalable. My commitment to excellence and my dedication to helping people grow and develop led me to train a successor to take over my work, ensuring that the platform continues to thrive and make a positive impact on people's lives.`

export const ikso: PortfolioItem = {
    id: "ikso",
    title: "Ikso",
    description,
    color: "red",
    role: "Freelance Web developer",
    startDate: "2012",
    endDate: "2016",
    tags: ["PHP", "Laravel", "Javascript", "JQuery", "Postgres", "Html", "Css", "Android", "Java"],
    links: [{ href: "https://ikso.nl/", label: "Website" }],
    work: [
        "Development of IKSO platform (PHP, Laravel, JQuery)",
        "Fleshing out concepts",
        "Development of IKSO apps for android and iOS (Objective-c, Java)",
        "Setting up and working with PostgreSQL database",
        "DevOps (VPS, Debian, Apache, PHP, Porstgres)",
        "Participating in discussions on the future of the platform",
        "Hiring and selection of new developers",
        "Coaching and training of junior developers",
        "Direct customer contact from feedback sessions",
    ],
    logo: {
        src: logo,
        alt: "Ikso logo",
    },
    cover: {
        src: cover,
        alt: "Ikso launch screen",
    },
    images: [
        {
            alt: "Ikso launch screen",
            src: screenshot1,
        },
        {
            alt: "Ikso main screen",
            src: screenshot2,
        },
        {
            alt: "Ikso timeline",
            src: screenshot3,
        },
    ],
}
