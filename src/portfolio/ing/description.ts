/* eslint-disable max-len */
import { PortfolioItem } from "@/models/PortfolioItem"

import logo from "./logo.png"
import screenshot1 from "./screenshot1.jpg"

// eslint-disable-next-line quotes
const name = "ING mobile banking"
const description = `During my time at ING, I had the opportunity to work on two key modules of the mobile banking app, namely the Onboarding and Enrollment modules. As the lead Android developer, I was responsible for ensuring the modules were developed with a focus on user experience, security, and high-quality technical design. In order to achieve this, I collaborated with cross-functional teams to establish clear communication and streamline workflows.

My role also involved coaching and mentoring the development team, while simultaneously playing an active role in hands-on coding to bring the new modules to life. By leveraging my technical expertise and problem-solving skills, I was able to identify and resolve issues in the development process, resulting in a highly functional and secure mobile banking app.`

export const ing: PortfolioItem = {
    id: "ing",
    title: name,
    description,
    color: "orange",
    role: "Engineering lead",
    startDate: "September 2021",
    endDate: "August 2022",
    tags: ["Lead developer", "Android", "Kotlin", "Architecture", "TDD", "CI", "Security", "Coaching"],
    links: [
        { href: "https://www.ing.nl/", label: "Website" },
        { href: "https://play.google.com/store/apps/details?id=com.ing.mobile&hl=en&gl=US", label: "Play store" },
    ],
    work: [
        "Development of new modules within the Mobile banking app",
        "Development of technical design and architecture focused on long-term quality",
        "Providing input and advice on future developments (both technical, functional, and team-related)",
        "Improving collaboration with other teams",
    ],
    logo: {
        src: logo,
        alt: `${name} logo`,
    },
    cover: {
        src: screenshot1,
        alt: `${name} example image`,
    },
    images: [
        {
            alt: `${name} example image`,
            src: screenshot1,
        },
    ],
}
