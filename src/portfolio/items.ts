/* eslint-disable max-len */

import { PortfolioItem } from "@/models/PortfolioItem"
import { Reference } from "@/models/Reference"
import { SkillSet } from "@/models/SkillSet"
import { TimelineItem } from "@/models/TimelineItem"
import { appical } from "./appical/description"
import { bridgefund } from "./bridgefund/description"
import { camarilla } from "./camarilla/description"
import { colect } from "./colect/description"
import { dynfos } from "./dynfos/description"
import { grofalex } from "./grofalex/description"
import { icm } from "./icm/description"
import { ikso } from "./ikso/description"
import { ing } from "./ing/description"
import { pfn } from "./pfn/description"
import { picturae } from "./picturae/description"
import { piggy } from "./piggy/description"
import { temper } from "./temper/description"
import { urly } from "./urly/description"
import { venturecoders } from "./venturecoders/description"
import { xxiyou } from "./xxiyou/description"

export const allPortfolioItems: PortfolioItem[] = [
    venturecoders,
    grofalex,
    dynfos,
    ing,
    colect,
    bridgefund,
    temper,
    appical,
    ikso,
    piggy,
    camarilla,
    urly,
    pfn,
    xxiyou,
    picturae,
    icm,
]

export const homeTimeline: TimelineItem[] = [
    { variant: "minorProjects", key: "1", items: [venturecoders, grofalex, dynfos] },
    { variant: "major", key: "2", portfolioItem: ing },
    { variant: "minorProjects", key: "3", items: [bridgefund, colect, piggy] },
    { variant: "major", key: "4", portfolioItem: temper },
    { variant: "major", key: "5", portfolioItem: appical },
    { variant: "minorProjects", key: "6", items: [camarilla, pfn, urly] },
    // { variant: "minorProjects", key: "7", items: [virate, popcards, xxiyou ] },
    { variant: "major", key: "8", portfolioItem: ikso },
    { variant: "minorProjects", key: "9", items: [xxiyou, picturae, icm] },
]

export const references: Reference[] = [
    {
        id: "floor-grofalex",
        name: "Floor",
        photo: grofalex.logo,
        company: "Grofalex",
        role: "General manager - Grofalex",
        review: "The software Django developed for us allowed us to scale up quickly and efficiently.",
        phone: "+31 6 43024592",
        email: "floor@grofalex.nl",
        projectId: grofalex.id,
    },
    // {
    //     id: "rik-dynfos",
    //     name: "Rik",
    //     photo: dynfos.logo,
    //     company: "Dynfos",
    //     role: "Functional manager - Dynfos",
    //     review: "Thanks to Django we now have a great app running in our production line.",
    //     phone: "+31 6 34488506",
    //     email: "r.temmink@dynfos.nl",
    //     projectId: dynfos.id,
    // },
    // {
    //     id: "rene-ing",
    //     name: "Rene Groot",
    //     photo: ing.logo,
    //     company: "ING",
    //     role: "Chapter lead Android",
    //     review: "Django's work getting the teams aligned was invaluable.",
    //     phone: "",
    //     email: "",
    //     projectId: ing.id,
    // },
    {
        id: "niek-bridgefund",
        name: "Niek",
        photo: bridgefund.logo,
        company: "Bridgefund",
        role: "Product owner - Bridgefund",
        review: "Django created our iOS and Android apps, his involvement from the start of the project was very valuable",
        phone: "+31 6 57991947",
        email: "niek@bridgefund.nl",
        projectId: bridgefund.id,
    },
    {
        id: "stefan-piggy",
        name: "Stefan",
        photo: piggy.logo,
        role: "Senior backend engineer - Piggy",
        company: "Piggy",
        review: "With Django's help we where able to quickly launch our platform on Android and expand our customer base.",
        phone: "+31 6 54680926",
        email: "stefan@piggy.nl",
        projectId: piggy.id,
    },
]

export const skillset: SkillSet[] = [
    {
        id: "lead",
        category: "Tech Lead",
        amount: "5+ years",
        skills: [
            { id: "leadership", title: "Leadership" },
            { id: "scrum", title: "Scrum" },
            { id: "management", title: "Project management" },
            { id: "planning", title: "Planning" },
            { id: "remote-teams", title: "Remote teams" },
            { id: "coaching", title: "Coaching" },
            { id: "public speaking", title: "Public speaking" },
            { id: "communication", title: "Communication" },
            { id: "teaching", title: "Teaching" },
        ],
    },
    {
        id: "architecture",
        category: "Architect",
        amount: "6+ years",
        skills: [
            { id: "architecture", title: "Software architect" },
            { id: "tdd", title: "TDD | OOP | FP | SOLID" },
            { id: "design-patterns", title: "Design patterns" },
            { id: "mvc", title: "MVC / MVVM / Frameworks" },
            { id: "ci", title: "CI/CD" },
            { id: "functional-design", title: "Technical and Functional design" },
            { id: "reactive", title: "Reactive programming" },
            { id: "cqrs", title: "CQRS" },
            { id: "microservices", title: "Microservices" },
        ],
    },
    {
        id: "front-end-mobile",
        category: "Frontend",
        amount: "8+ years",
        skills: [
            { id: "mobile", title: "Native iOS/Android" },
            { id: "objective-c", title: "Objective-c" },
            { id: "swift", title: "Swift" },
            { id: "kotlin", title: "Kotlin" },
            { id: "java", title: "Java" },
            { id: "javascript", title: "Javascript" },
            { id: "react", title: "React" },
            { id: "vue", title: "Vue" },
            { id: "nextjs", title: "NextJS" },
        ],
    },
    {
        id: "backend",
        category: "Backend",
        amount: "6+ years",
        skills: [
            { id: "node", title: "Javascript / NodeJS" },
            { id: "python", title: "Python / Flask" },
            { id: "postgres", title: "Postgres" },
            { id: "mysql", title: "Mysql" },
            { id: "elastic", title: "Elastic search" },
            { id: "mongoDb", title: "MongoDB" },
            { id: "google-cloud", title: "Google Cloud" },
            { id: "aws", title: "AWS" },
            { id: "php", title: "PHP / Laravel" },
        ],
    },
]
