/* eslint-disable max-len */
import { PortfolioItem } from "@/models/PortfolioItem"

import logo from "./logo.png"
import screenshot1 from "./screenshot1.png"

// eslint-disable-next-line quotes
const name = "Push for news"
const description = "As the main iOS developer, I developed the app from scratch with a focus on creating a seamless user experience and visually appealing design. I collaborated with the design team to incorporate various smooth animations that enhanced the overall look and feel of the app."

export const pfn: PortfolioItem = {
    id: "pfn",
    title: name,
    description,
    color: "red",
    role: "iOS App",
    startDate: "January 2016",
    endDate: "April 2016",
    tags: ["Swift", "iOS", "Google news API"],
    work: [

    ],
    logo: {
        src: logo,
        alt: `${name} logo`,
    },
    cover: {
        src: screenshot1,
        alt: `${name} example image`,
    },
    images: [
        {
            alt: `${name} screenshot 1`,
            src: screenshot1,
        },
    ],
}
