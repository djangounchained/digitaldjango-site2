/* eslint-disable max-len */
import { PortfolioItem } from "@/models/PortfolioItem"

import logo from "./logo.png"
import screenshot1 from "./screenshot1.jpg"

// eslint-disable-next-line quotes
const name = "Picturae"
const description = `As a Junior Engineer at Picturae, I was involved in the development of Memorix Maior, a web application designed to manage and create digital collections for cultural heritage. Working within a team, my responsibilities included coding in PHP, jQuery, and JavaScript, as well as managing and optimizing the database using PostgreSQL and SQL.

Additionally, I worked with the Zend framework and focused on building and optimizing web applications, including collection websites. This experience allowed me to expand my technical knowledge and develop my skills as a junior developer, while also gaining valuable experience working in a team environment on a complex project. Overall, my contributions helped ensure the success of the Memorix Maior platform for Picturae and its clients.`

export const picturae: PortfolioItem = {
    id: "picturae",
    title: name,
    description,
    color: "orange",
    role: "Junior Web developer",
    startDate: "2010",
    endDate: "2012",
    tags: ["Web developer", "PHP", "Javascript", "JQuery", "Postgres", "Zend framework"],
    links: [
        { href: "https://picturae.com/", label: "Website" },
    ],
    work: [
        "Development of websites for clients with Joomla",
        "Development of the Memorix maior systeem with PHP, Javascript and Postgres",
    ],
    logo: {
        src: logo,
        alt: `${name} logo`,
    },
    cover: {
        src: screenshot1,
        alt: `${name} screenshot`,
    },
    images: [
        {
            alt: `${name} screenshot`,
            src: screenshot1,
        },
    ],
}
