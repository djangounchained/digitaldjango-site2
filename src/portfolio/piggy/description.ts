/* eslint-disable max-len */
import { PortfolioItem } from "@/models/PortfolioItem"

import logo from "./logo.png"
import cover from "./cover.png"

import screenshot1 from "./screenshot1.png"
import screenshot2 from "./screenshot2.png"
import screenshot3 from "./screenshot3.png"
import screenshot4 from "./screenshot4.png"

// eslint-disable-next-line quotes
const name = "Piggy"
const description = `Piggy is a loyalty program that makes saving points at your favorite stores fun and easy. As the Lead Android Developer, I was responsible for developing and managing two Android applications. The first app was designed for use on Android tablets in stores across the Netherlands and Germany. The app features image recognition technology that allows users to scan their loyalty card, as well as integration with existing cash register systems to automatically save points. The second app, which has been downloaded over 100K times, is used by customers to view their savings and promotions from stores, and even link their bank accounts to automatically save points without scanning their card.

As the lead developer, I was responsible for everything from architecture and coding to workflow and training of new team members. I developed a solid workflow that allowed us to improve productivity by streamlining our development process. I also trained new team members to ensure that they were equipped with the necessary skills to be productive in their roles. Overall, my contributions helped to make Piggy a successful and user-friendly platform.`

export const piggy: PortfolioItem = {
    id: "piggy",
    title: name,
    description,
    color: "orange",
    role: "App development",
    startDate: "Februari 2020",
    endDate: "March 2021",
    tags: ["Kotlin", "Android", "Choaching", "Team lead", "Push notifications", "CI", "Unit testing", "Image recognition", "Full text search", "Adyen integration"],
    links: [
        { href: "https://www.piggy.eu/nl", label: "Website" },
        { href: "https://play.google.com/store/apps/details?id=nl.piggy.klantenapp", label: "Play store" },
    ],
    work: [],
    logo: {
        src: logo,
        alt: `${name} logo`,
    },
    cover: {
        src: cover,
        alt: `${name} example image`,
    },
    images: [
        {
            alt: `${name} screenshot 1`,
            src: screenshot1,
        },
        {
            alt: `${name} screenshot 2`,
            src: screenshot2,
        },
        {
            alt: `${name} screenshot 3`,
            src: screenshot3,
        },
        {
            alt: `${name} screenshot 4`,
            src: screenshot4,
        },
    ],
}
