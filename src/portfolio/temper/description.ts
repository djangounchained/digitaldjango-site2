/* eslint-disable max-len */
import { PortfolioItem } from "@/models/PortfolioItem"

import logo from "./logo.png"
import screenshot1 from "./screenshot1.png"
import screenshot2 from "./screenshot2.png"

// eslint-disable-next-line quotes
const name = "Temper"
const description = `Temper is an innovative digital bulletin board where clients post jobs and freelancers create profiles to market their services. Every week, thousands of freelancers and organizations find suitable jobs in the logistics, retail, and hospitality industries through Temper.

As the Engineering Lead for the freelancer section of the Temper application, Mr. Witmer was responsible for leading an international development team of ten people with different skillsets, including some based in Sri Lanka. His main responsibilities included overseeing the development of the freelancer section of the Temper application, ensuring quality control, leading the team, and coordinating with other departments within the company. He also played a crucial role in implementing new features and ensuring that the application was scalable and efficient. Mr. Witmer's excellent leadership skills and expertise in software development made him an essential member of the Temper team, and his contributions were instrumental in the success of the platform.`

export const temper: PortfolioItem = {
    id: "temper",
    title: name,
    description,
    color: "red",
    role: "Engineering lead",
    startDate: "January 2019",
    endDate: "Oktober 2020",
    tags: ["Lead developer", "iOS", "Swift", "Android", "Kotlin", "Architecture", "Coaching", "Push", "Realm db", "Javascript", "React", "Circle CI"],
    links: [
        { href: "https://www.appical.net", label: "Website" },
    ],
    work: [
        "Development of technical design and architecture focused on long-term quality",
        "Hands on coding with technologies such as Javascript, Vue, Vuex, Tailwind, PHP, Laravel, and MySQL",
        "Establishment of continuous integration and automated releases using Codeship",
        "Deployments and hosting through Cloudflare",
        "Improving retention through A/B testing with Optimizely",
        "Working with big data and analytics to further improve the platform",
        "Refactoring legacy code",
        "Establishment of development workflow including code reviews, QA, and testing with the goal of maintaining high app quality and enabling easy team scaling in the future",
        "Promoting/encouraging TDD mentality within the team",
        "Streamlining communication with other teams",
        "Recruitment and selection of new team members",
        "Coaching and training of the development team",
        "Providing input and advice on future developments (technical, functional, and team-oriented)",
    ],
    logo: {
        src: logo,
        alt: `${name} logo`,
        background: "bg-purple",
    },
    cover: {
        src: screenshot1,
        alt: `${name} in action`,
    },
    images: [
        {
            alt: `${name} in action`,
            src: screenshot1,
        },
        {
            alt: `${name} screenshots`,
            src: screenshot2,
        },
    ],
}
