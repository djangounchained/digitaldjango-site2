/* eslint-disable max-len */
import { PortfolioItem } from "@/models/PortfolioItem"

import logo from "./logo.png"
import screenshot1 from "./screenshot1.jpg"

// eslint-disable-next-line quotes
const name = "Urly"
const description = "I developed a comprehensive iOS app that keeps users informed about dance events in the Netherlands, providing timely notifications of early bird ticket sales. My responsibility encompassed the entire platform, from ideation to implementation. In addition to the mobile app I also created a backend system and api for the app. I used a serverless environment with Google Cloud functions, which facilitated a robust backend system built using Python and an intuitive admin panel constructed with React. My expertise and dedication led to the successful completion of this project."

export const urly: PortfolioItem = {
    id: "urly",
    title: name,
    description,
    color: "purple",
    role: "iOS App",
    startDate: "Februari 2015",
    endDate: "April 2016",
    tags: ["Swift", "iOS", "Push notifications", "Python", "Flask", "Postgres", "Javascript", "React", "Html", "CSS", "Google cloud functions"],
    work: [

    ],
    logo: {
        src: logo,
        alt: `${name} logo`,
    },
    cover: {
        src: screenshot1,
        alt: `${name} example image`,
    },
    images: [
        {
            alt: `${name} screenshot 1`,
            src: screenshot1,
        },
    ],
}
