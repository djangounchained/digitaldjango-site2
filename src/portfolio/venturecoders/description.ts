/* eslint-disable max-len */
import { PortfolioItem } from "@/models/PortfolioItem"

import logo from "./logo.png"
import screenshot1 from "./screenshot1.png"
import screenshot2 from "./screenshot2.png"
import speaking from "./speaking.jpg"

// eslint-disable-next-line quotes
const name = "VentureCoders"
const description = `As co-founder and teacher at VentureCoders, I was responsible for developing educational materials and a course platform as well as giving lectures, classes and talks about software engineering. We offer beginner courses in creating Android apps as well as advanced courses tailored for businesses.

One such course was focused on native mobile development, teaching engineers how to efficiently create a native app. Another course focused on automated testing, covering topics such as writing good tests, what to test and what not to test, and establishing a workflow to write easily testable code.

I also provide training in front-end and microservice architecture, team collaboration, and coaching. In addition, we offer customized training programs designed specifically to meet the needs of individual businesses. Through these courses, we aim to empower developers with the skills and knowledge necessary to excel in their roles and advance their careers.`

export const venturecoders: PortfolioItem = {
    id: "venturecoders",
    title: name,
    description,
    color: "purple",
    role: "Co-founder and teacher",
    startDate: "August 2022",
    endDate: "Now",
    tags: ["Startup", "Course development", "React", "NextJs", "Kotlin", "Firebase"],
    links: [
        { href: "http://venturecoders.nl/", label: "Website" },
    ],
    work: [
        "Developing the idea to a functional and technical design",
        "Development of UI/UX",
        "Development of technical design and architecture focused on long-term quality",
        "Stack: React, NodeJS, Postgres, Prisma, Vercel, AWS",
        "Managing the engineering team",
        "Assistance in writing complex SQL queries",
        "Project management",
        "Providing advice and recommendations on future developments",
    ],
    logo: {
        src: logo,
        alt: `${name} logo`,
    },
    cover: {
        src: speaking,
        alt: "Giving a lecture",
    },
    images: [
        {
            alt: `${name} screenshot 1`,
            src: screenshot2,
        },
        {
            alt: `${name} screenshot`,
            src: screenshot1,
        },
        {
            src: speaking,
            alt: "Giving a lecture",
        },
    ],
}
