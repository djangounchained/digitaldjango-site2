/* eslint-disable max-len */
import { PortfolioItem } from "@/models/PortfolioItem"

import logo from "./logo.png"
import screenshot1 from "./screenshot1.png"
import screenshot2 from "./screenshot2.jpg"

// eslint-disable-next-line quotes
const name = "Xxiyou"
const description = "An app that would allow shoppers to get discounts on selected products. The client was ultimately unable to make good deals with shops and failed to attract a user base. I built the iOS and Android apps from the ground up and worked on various iterations of the platform. This was back when Android was at version 2.2 and iOS 5 just came out."

export const xxiyou: PortfolioItem = {
    id: "xxiyou",
    title: name,
    description,
    color: "purple",
    role: "Freelance app developer",
    startDate: "2014",
    endDate: "2013",
    tags: ["iOS", "Swift", "Android", "Kotlin"],
    links: [],
    work: [],
    logo: {
        src: logo,
        alt: `${name} logo`,
        background: "bg-purple",
    },
    cover: {
        src: screenshot1,
        alt: `${name} example image`,
    },
    images: [
        {
            alt: `${name} example image`,
            src: screenshot1,
        },
        {
            alt: `${name} screenshots`,
            src: screenshot2,
        },
    ],
}
