/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./src/pages/**/*.{js,ts,jsx,tsx}",
        "./src/components/**/*.{js,ts,jsx,tsx}",
        "./src/app/**/*.{js,ts,jsx,tsx}",
    ],
    theme: {
        fontFamily: {
            title: ["Montserrat", "sans-serif"],
            body: ["Roboto", "sans-serif"],
        },

        colors: {
            transparent: "transparent",
            current: "currentColor",
            black: {
                DEFAULT: "#000000",
                50: "rgba(0, 0, 0, 0.5)",
            },
            white: "#ffffff",
            gray: {
                50: "#F3F3EC",
                100: "#e7e7d9",
            },
            orange: {
                DEFAULT: "#FA824C",
                70: "rgba(250, 131, 76, 0.7)",
            },
            purple: {
                DEFAULT: "#444054",
                50: "rgba(68, 64, 84, 0.5)",
            },
            red: "#FF6663",
        },

        fontSize: {
            sm: "0.938rem",
            md: "1rem",
            "md-desktop": "1.125rem",
            lg: "1.5rem",
            xl: "2rem",
            xxl: "3rem",
        },
        lineHeight: {
            none: "1.5rem",
            sm: "1rem",
            md: "2rem",
            lg: "3rem",
            xl: "4rem",
        },

        extend: {
            height: {
                header: "calc(100vh - 3.5rem)",
            },
            // backgroundImage: {
            //     'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
            //     'gradient-conic':
            //     'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
            // },
        },
    },

    safelist: [
        "bg-red",
        "bg-purple",
        "bg-orange",
    ],

    plugins: [],
}
